import {Row, Col, Button} from 'react-bootstrap'
import {useState} from 'react'

export default function CourseCard({courseProp}){

	console.log(courseProp)

// Object destructuring
	const {name, description, price} = courseProp
	// Symtax: const {properties} = coursePropname 

//Array destructuring
// count(initial value) setCount(function)
const [count, setCount] = useState(0)
// Syntax: const[getter, setter] = useState(initialValue)

//Hook used is useState - to store the state

function enroll(){
	setCount(count + 1);
	console.log('Enrollees' + count);

	if(count == 30){
		alert('No more seats');
	}
}


	return(
		<Row>
			<Col className = "p-5">
				<h1>{name}</h1>
				<h3>Description</h3>
				<p>{description}</p>

				<h3>Price</h3>
				<p>{price}</p>
				<p>Enrollees: </p>
				<p>{count} Enrollees</p>
				<Button variant = "primary" onClick= {enroll}>Enroll</Button>
			</Col>
		</Row>
		
	)
}
