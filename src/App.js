import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home'
import './App.css';
import Register from './pages/Register'
import Login from './pages/Login'

function App() {
  return (
  <Fragment>
    <AppNavbar/>
    <Container>    
     {/* <Banner/>
      <Highlights/>*/}
      <Login/>
      {/*<Register/>
      <Home/>
      <Courses/>*/}
    </Container>
  </Fragment>
  );
}

export default App;
